# Projet_6


## Getting started

L'association vous demande de réaliser un algorithme de détection de la race du chien sur une photo, afin d'accélérer leur travail d’indexation.

Vous avez peu d’expérience sur le sujet, vous décidez donc de contacter un ami expert en classification d’images.

Il vous conseille dans un premier temps de pré-processer des images avec des techniques spécifiques (e.g. whitening, equalization, éventuellement modification de la taille des images) et de réaliser de la data augmentation (mirroring, cropping...).

Ensuite, il vous incite à mettre en œuvre deux approches s’appuyant sur l’état de l’art et l’utilisation de CNN (réseaux de neurones convolutionnels), que vous comparerez en termes de temps de traitement et de résultat :

Une première en réalisant votre propre réseau CNN, en vous inspirant de réseaux CNN existants. Prenez soin d'optimiser certains hyperparamètres (des layers du modèle, de la compilation du modèle et de l’exécution du modèle)
Une deuxième en utilisant le transfer learning, c’est-à-dire en utilisant un réseau déjà entraîné, et en le modifiant pour répondre à votre problème.
Concernant le transfer learning, votre ami vous précise que :

Une première chose obligatoire est de réentraîner les dernières couches pour prédire les classes qui vous intéressent seulement.
Il est également possible d’adapter la structure (supprimer certaines couches, par exemple) ou de réentraîner le modèle avec un très faible learning rate pour ajuster les poids à votre problème (plus long) et optimiser les performances.
Ressources de calcul
Votre ami vous alerte sur le fait que l'entraînement (même partiel) d'un réseau de neurones convolutionnels est très gourmand en ressources. Si le processeur de l’ordinateur un peu ancien de l’association ne suffit pas, il vous propose plusieurs solutions :

Limitez le jeu de données, en ne sélectionnant que quelques classes (races de chiens), ce qui permettra déjà de tester la démarche et la conception des modèles, avant une éventuelle généralisation.
Utilisez la carte graphique de l’ordinateur en tant que GPU (l'installation est un peu fastidieuse, et l'ordinateur est inutilisable le temps du calcul).


***

# Editing this README

Ressources complémentaires
1) Prétraitement des images
   Présentation d’une technique de preprocessing : le whitening.
   Ce tutoriel présente les concepts de data augmentation. À vous de voir si cela peut améliorer vos performances sur ce projet !
   Présentation d’une autre technique de preprocessing : l’equalization.
   Deux cours sur le pre-processing d’images : le premier est succinct et illustre les techniques, c’est une bonne première approche ; le second fournit beaucoup de détails qui permettent de mieux appréhender les cas d’utilisation des différents filtres, notamment.
2) CNN et transfer learning
   Guide d’utilisation de Google Colaboratory avec GPU.
   Une présentation du transfer learning, ses avantages, les cas d’application et un exemple de code en Keras.


## Livrables

Un notebook Python (non cleané, pour comprendre votre démarche).
Un programme Python qui prend une image (array) en entrée et retourne la race la plus probable du chien présent sur l'image.
Votre support de présentation à destination du bénévole qui gère la base de données, qui devra déployer en production la solution que vous préconisez.


Pour faciliter votre passage devant le jury, déposez sur la plateforme, dans un dossier zip nommé “Titre_du_projet_nom_prénom”, votre livrable nommé comme suit : Nom_Prénom_n° du livrable_nom du livrable_date de démarrage du projet. Cela donnera :

Nom_Prénom_1_notebook_mmaaaa
Nom_Prénom_2_programme_mmaaaa
Nom_Prénom_3_presentation_mmaaaa
Par exemple, votre premier livrable peut être nommé comme suit : Dupont_Jean_1_notebook_012022.


Votre soutenance durera 25 minutes, découpées ainsi (à titre indicatif) :

5 min - Présentation de la problématique, du cleaning effectué, du feature engineering et de l'exploration
10 min - Présentation des différentes pistes de modélisation effectuées
10 min - Présentation du modèle final sélectionné (pour chaque approche) ainsi que des performances et améliorations effectuées
5 à 10 minutes de questions-réponses








# Note d’accompagnement
L’intention de ce projet est de mettre en œuvre des algorithmes de Deep Learning dans un contexte de traitement d’images (computer vision). Il s’agit pour l’étudiant de maîtriser le pre-processing d’images, les concepts de Deep Learning, en particulier les CNN, leur structure, la problématique d’optimisation des différents types hyperparamètres.
C’est aussi l’opportunité de savoir ré-utiliser, au travers du Transfer Learning, des traitements optimisés et basés sur des sources plus larges d’images. Cette approche se généralise, car elle apporte un gain de pertinence et de temps à de nombreuses problématiques autour de la « computer vision » (détection, localisation d’objets, voiture autonome, …).

L’étudiant devra pouvoir comparer les 2 approches (propre réseau CNN, transfer Learning), en termes de complexité de conception, de pertinence de résultat et de temps de traitement.

N’hésitez-pas à inciter l’étudiant de mettre en place plusieurs techniques de pre-processing d’images, et de mesurer leur apport dans les résultats des modèles.

De la même manière, incitez l’étudiant à mettre en œuvre plusieurs techniques de data augmentation, idéalement de data augmentation via Keras ImageDataGenerator (en pre-processing, ou génération à la volée en cours de training via fit_generator), afin de renforcer le jeu de données dans un contexte de nombre d’images limité par classe.

L’étudiant doit pouvoir également réaliser de nombreuses simulations afin d’optimiser les modèles (ajustement du modèle de layers, optimisation des hyperparamètres), pour s’approprier les concepts et comprendre les impacts. Ainsi si besoin, il pourra limiter le nombre de classes (races de chiens), afin de réduire sensiblement les temps de traitement.

Évaluation des compétences

Sélectionner un modèle d'apprentissage Deep Learning adapté à une problématique métier
La compétence est validée si :

❒ L'intérêt de construire un modèle de type CNN, adapté au contexte de classification d'images est justifié

❒ L'intérêt de de compléter le modèle CNN spécifique par un CNN pré-entrainé (Transfer Learning), afin d'exploiter la connaissance extraite de millions d'images est justifié


## Mettre en place un modèle de Deep Learning

La compétence est validée si :

❒ Au moins 2 modèles CNN spécifiques (choix différents de layers), en s'inspirant de modèles existants ont été mis en œuvre

❒ Au moins 1 modèle CNN transfer Learning tels que VGG16, ResNet50, ou Inception ResNetV2 ont été mis en œuvre

❒ Des librairies spécialisées comme KERAS (pour élaborer les modèles de CNN) ont été utilisées


## Évaluer les performances d’un modèle de Deep Learning

La compétence est validée si :

❒ Les données ont été séparées en train / validation pour les évaluer de façon pertinente et détecter l'overfitting

❒ Une métrique adaptée a été choisie (accuracy score du jeu de validation)


## Adapter les paramètres d'un modèle de Deep Learning afin de l’améliorer

La compétence est validée si :

❒ Les hyperparamètres des layers définis ont été adaptés (kernel size, dropout, méthode d'activation des layers cachés, méthode

d'activation du layer final, ...)

❒ Les hyperparamètres de compilation du modèle ont été adaptés (optimizer, loss, ...)

❒ Les hyperparamètres d'exécution du modèle ont été adaptés (batch size, nombre d'epochs, ...)

❒ Les résultats ont été comparés de manière automatique selon les différents modèles et hyperparamètres


## Transformer les variables pertinentes d'un modèle de Deep Learning

La compétence est validée si :

❒ Des librairies spécialisées ont été utilisées (ex : openCV)

❒ Au moins un traitement d'images a été réalisé tels que cropping, mirroring, whitening, contraste, equalization, débruitage, redimensionnement

❒ L'intérêt de ces étapes est démontré par l’étudiant (montrer un exemple "avant/après" pour un des traitements)

❒ Les apports de la data augmentation sont expliqués (principe, intérêt et nécessité pour ce dataset ?)

❒ Un exemple de data augmentation a été mis en œuvre (exemple : mirroring, cropping, Keras ImageDataGenerator)


```
cd existing_repo
git remote add origin https://gitlab.com/ing_ml/projet_6.git
git branch -M main
git push -uf origin main
```


